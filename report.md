# INF 727 project of a distributed MapReduce

This project documentation will present the application, its architecture, the tooling and design choices, as well as discuss the performances on working examples and finally highlight some of the limitation of the current implementations. 

The main challenge was to implement a simple but general purpose distributed computation framework, rather than a specific implementation of a distributed MapReduce.


# 0. Usefull information and resources

REST API where implemented using <a href="https://fastapi.tiangolo.com/">FastAPI</a> for routes and <a href="https://pydantic-docs.helpmanual.io/">Pydantic</a> for Data Models. Both those packages although relatively new to the python ecosystem, offer exceptional performance (for a high level language), benefit from the latest language features around typing and have good overall documentation.
API documentation leverage OpenAPI schemas and uses Swagger UI and ReDoc.

Project documentation was generated from Markdown source file using <a href="https://www.sphinx-doc.org/en/master/">Sphinx</a>, and is served with the most basic http server hosted on the Master node.

The application is hosted on 4 <a href="https://www.digitalocean.com/">Digital Ocean</a> cloud based virtual machines:

| Node   |      Host      |
|----------|:-------------:|
| Master | http://164.92.219.247:8000/ |
| Worker 1 | http://164.92.219.216:8000/ |
| Worker 2 | http://164.92.219.239:8000/ |
| Worker 3 | http://164.92.219.238:8000/ |

```console
{url}/docs gives access to endpoint API documentation.
```
All project source files are available in the following public repository.

# 1. Application architecture

Donkey borrows extensively from ideas seen in Apache Hadoop and deploys a master worker architecture.
The distributed application runs with one master node (which for now remains a single point of failure) and several worker nodes.
Each node is a web server implementing a rest api, such api is used for both node to node and client to master node interactions.
Each worker node is also running a simple task queue to run tasks asynchronously in an event loop to stay responsive to http request.

Both master and worker API were designed to have similar API, this intended goal was to make the inner-working of the application easier to grasp.
But this has turned out to be cumbersome and led to some duplicated logic (more of this in discussed in the limitation section)
 
Overview of the application architecture:
<br>
<p align="center"><img src="diagram.png"></p>

On the client side the application offers distributed computating framework, built on groups of functionalities:

**A distributed file system**: Clients can submit text files intended for processing to the master node that will split
and distribute those asynchronously to worker nodes.  
Here are a set of example http requests demonstrating such process.  
*Client to Master Node requests:*
```console
curl -X 'POST' \
  'http://164.92.219.247:8000/fs' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'file=@civil.txt;type=text/plain'
```
*Master Node to Worker Node Requests:*

```console
curl -X 'POST' \
  'http://164.92.219.216:8000/fs' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'file=@civil.txt-block-00001;type=text/plain'
```
The Master node is keeping track of distributed block locations in binary file stored locally on disk.
Content of the block file:



**Distributed computing**: Clients can submit python source files to be executed, such scripts must implement a compute with the following signature:
```python
# *args for any combination of positional arguments
def compute(input_file_path, output_file_path, *args):
  pass
```
Submitted scripts are parsed and the AST is checked on the fly for a compute method (Scripts are trusted and no other checks are performed as they do not run in a sandboxed environment).

This is performed on the *jobs* endpoint with similar http requests:

```console
curl -X 'POST' \
  'http://164.92.219.247:8000/jobs' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'file=@char_count.py'
```

Finally we can run tasks on files in file system using the *tasks* endpoint, like in the following example:

```console
curl -X 'PUT' \
  'http://164.92.219.247:8000/tasks?job_names=char_count&file_name=forestier.txt&aggregation_function=sum' \
  -H 'accept: application/json'
```

The project is deployed in /opt/donkey of the host machines with the following folder structure:

```console
.
├── var         
│   ├── data    --> Holds file system data on workers (file blocks)
│   ├── master  --> Contains master specific file, block journal (recording where each block in stored)
│   ├── scripts --> Python scripts (jobs) deployed to each worker node 
├── var         --> Directory containing the python virtual environment
├── slave.py    --> Application file, this file is the input to uvicorn webserver for worker nodes.
├── master.py   --> Application file, this file is the input to uvicorn webserver for the master node.
└── req.txt     --> Text file listing all the required dependencies (used for the pip install step of application deployment) 
    

```

 The block size was initially set to a fixed (but configurable) value, but was later changed to a dynamic value calculated as
 b = file_size / #worker_nodes
 This approach has shown a better computing performance. 
 
# 2. Deployment

The deployment of all nodes on the hosts is fully automated using <a href="">Fabric</a> and python, the steps of the deployment script are described in the comments in the source code below:

**deploy.py**
```python
from fabric import Connection
from fabric.transfer import Transfer
import pathlib

TARGET_DIR = "/opt/donkey/"
hosts = [
    "164.92.219.247",  # master node
    "164.92.219.216",  # worker node 1
    "164.92.219.239",  # worker node 2
    "164.92.219.238",  # worker node 3
]


# Remote command execution via ssh logic is encapsulated in this Node class
class Node:
    def __init__(self, user, host):
        self._connection = Connection(f'{user}@{host}')

    def run(self, cmd: str, hide=False):
        return self._connection.run(cmd, hide=hide)

    def get(self, src, dest):
        return Transfer(self._connection).get(src, dest)

    def put(self, src, dest):
        self._connection.run(f'mkdir -p {dest.parent.as_posix()}')
        return Transfer(self._connection).put(src, dest.as_posix())


nodes = [Node("root", host) for host in hosts]

print('1- Connecting to remote hosts')
print('_____________________________')
for node in nodes:
    print(f'Connected to host {node.run("hostname").stdout.strip()}')
print('✓ Done')


# Purging previous deployment and creating the target directory structure
print('2- Creating directory structure')
print('_______________________________')
for node in nodes:
    node.run(f'rm -rf {TARGET_DIR}')
    print(f'On host {node.run("hostname").stdout.strip()}')
    node.run(
        f"sudo mkdir -p {TARGET_DIR}static {TARGET_DIR}templates {TARGET_DIR}var/data {TARGET_DIR}var/master {TARGET_DIR}var/scripts"
    )
print('✓ Done')

# Copying application files on the remote hosts
print('3- Copying project executable to remote hosts')
print('______________________________________________')
script_files = ["slave.py", "master.py", "req.txt", "config.py"]
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    for file in script_files:
        node.put(
            pathlib.Path(__file__).parent.joinpath(file),
            pathlib.Path(TARGET_DIR).joinpath(file),
        )
print('✓ Done')

# Copying static resources and html templates
print('4- Copying project static assets to remote hosts')
print('_________________________________________________')
static_dirs = ["static/donkey.png", "static/favicon.png", "templates/index.html"]
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    for _dir in static_dirs:
        node.put(
            pathlib.Path(__file__).parent.joinpath(_dir),
            pathlib.Path(TARGET_DIR).joinpath(_dir),
        )
print('✓ Done')

# Printing the content of remote hosts /opt/donkey folder to stdout
print('5- Checking the content of the deployment directory')
print('___________________________________________________')
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    print(node.run(f"ls {TARGET_DIR}").stdout)
print('✓ Done')

# Creating a python virtual environment, activating it and installing the project dependencies
print('6- Creating python virtual environment and installing dependencies')
print('_________________________________________________________________')
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    node.run("sudo apt-get update -y")
    node.run("sudo apt install python3.8-venv -y")
    node.run(f"python3 -m venv {TARGET_DIR}env")
    node.run(f"source {TARGET_DIR}/env/bin/activate")
    node.run(f"{TARGET_DIR}env/bin/python -m pip install -r {TARGET_DIR}req.txt")
    node.run(f"{TARGET_DIR}env/bin/python -m pip list")
print('✓ Done')

# Reloading and starting web server services on hosts
print('7- Starting server processes as services')
print('___________________________________________________')
for node in nodes:
    node.run("sudo systemctl daemon-reload")
    node.run("sudo systemctl start donkey.service")
print('✓ Done')
```

**/etc/systemd/system/donkey.service**
```console
[Unit]
Description=Donkey Worker

[Service]
ExecStart=/opt/donkey/env/bin/python /opt/donkey/slave.py
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
# 3. Interacting with the application
Donkey comes with a basic client library implemented in `donkey_client.py`, aside from the swagger api doc, donkey_client is the prefered way to interact with the application.

Here are a few example documenting its usage.

*Querying for some cluster information*
```python
from donkey_client import DonkeyClient
MASTER_NODE: str = r'http://164.92.219.247:8000'
dc = DonkeyClient(MASTER_NODE)
dc.cluster_info()
```
```console
{'http://164.92.219.216:8000/': 1, 'http://164.92.219.239:8000/': 2, 'http://164.92.219.238:8000/': 3}
```
We start by defining a few local file system locations, donkey client comes with a few example:
```python
from pathlib import Path
SCRIPTS_PATH : Path= Path(r'/path/to/project/examples/scripts')
DATA_PATH : Path= Path(r'/path/to/project/examples/data')
```
Then initialize the client:
```python
from donkey_client import DonkeyClient
dc : DonkeyClient = DonkeyClient(MASTER_NODE)
```
Let's try submitting an input data file from the examples folder:

```python
dc.submit_file(DATA_PATH / "civil.txt")
```
Return a json response with the newly created file distributed across:
```console
{'blocks': [['civil.txt-block-00001', 'http://164.92.219.216:8000/'],
            ['civil.txt-block-00002', 'http://164.92.219.239:8000/'],
            ['civil.txt-block-00003', 'http://164.92.219.238:8000/']],
 'file_name': 'civil.txt'}
```

We can then submit a python script with a compute method, that will be run in the distributed application:
```python
dc.submit_job(SCRIPTS_PATH / "map.py")
```
This should return the file name and the worker nodes it was deployed to:
```console
{'job_name': 'map.py',
 'worker_nodes': ['http://164.92.219.216:8000/',
                  'http://164.92.219.239:8000/',
                  'http://164.92.219.238:8000/']}
```
Finally running a task is done with the following call:

```python
dc.run_task("civil.txt",  "map")
```

Will return a collection of task objects, one per worker node
````console
[{'completed': False,
  'duration': None,
  'end': None,
  'file': 'civil.txt',
  'job': 'map',
  'output': 'map_civil.txt',
  'output_block': 'map_civil.txt-block-00001',
  'start': '2022-01-03T21:19:39.715914',
  'uid': 'map_8655ef7918',
  'worker': 'http://164.92.219.216:8000/'},
 {'completed': False,
  'duration': None,
  'end': None,
  'file': 'civil.txt',
  'job': 'map',
  'output': 'map_civil.txt',
  'output_block': 'map_civil.txt-block-00002',
  'start': '2022-01-03T21:19:39.784425',
  'uid': 'map_360338e060',
  'worker': 'http://164.92.219.239:8000/'},
 {'completed': False,
  'duration': None,
  'end': None,
  'file': 'civil.txt',
  'job': 'map',
  'output': 'map_civil.txt',
  'output_block': 'map_civil.txt-block-00003',
  'start': '2022-01-03T21:19:39.826553',
  'uid': 'map_bbbe986b87',
  'worker': 'http://164.92.219.238:8000/'}]
````
The following can be use to retrieve task execution status from the server
```python
dc.retrieve_task_status()
```
```console
{'map_360338e060': {'completed': True,
                    'duration': 1084,
                    'end': '2022-01-03T21:11:32.843743',
                    'file': 'civil.txt',
                    'job': 'map',
                    'output': 'map_civil.txt-block-00002',
                    'start': '2022-01-03T21:11:31.760133',
                    'uid': 'map_360338e060',
                    'worker': 'http://164.92.219.216:8000/'},
 'map_8655ef7918': {'completed': True,
                    'duration': 1133,
                    'end': '2022-01-03T21:11:32.837744',
                    'file': 'civil.txt',
                    'job': 'map',
                    'output': 'map_civil.txt-block-00001',
                    'start': '2022-01-03T21:11:31.705241',
                    'uid': 'map_8655ef7918',
                    'worker': 'http://164.92.219.239:8000/'},
 'map_bbbe986b87': {'completed': True,
                    'duration': 1063,
                    'end': '2022-01-03T21:11:32.909801',
                    'file': 'civil.txt',
                    'job': 'map',
                    'output': 'map_civil.txt-block-00003',
                    'start': '2022-01-03T21:11:31.846573',
                    'uid': 'map_bbbe986b87',
                    'worker': 'http://164.92.219.238:8000/'}}
```
The output of a given task is stored in the application's file system under the task.output property of the task object.
It can be retrieved as follows:
```python
dc.retrieve_task_status("map_civil.txt")
```
That return a json file with computation output in the content attribute:
```console
{'content': {'': 1,
             '000': 1,
             '1': 1,
             '10': 1,
             '100': 1,
             '1000': 1,
             '1001': 1,
             '1002': 1,
             [...]
 'content_type': 'json',
 'file_name': 'map_civil.txt'}
```
The donkey client also provide a way to chain task execution, here is a complete code snippet to run a map, shuffle and
reduce task:
```python
dc = DonkeyClient(MASTER_NODE)

dc.submit_file(DATA_PATH / "civil.txt")
dc.submit_job(SCRIPTS_PATH / "map.py")
dc.submit_job(SCRIPTS_PATH / "shuffle.py")
dc.submit_job(SCRIPTS_PATH / "reduce.py")

r = dc.run_task_chain("civil.txt", ["map", "shuffle", "reduce"])
output_file = r[0]['output']
output = dc.retrieve_files(output_file)
```

Call to Tasks endpoint a non-blocking, the client receives a data strucutre with task information that can be used later to query for status.
With this in mind Task synchronisation is achieved at the client level, by making sure the previous task has finished before running the next task. As show above this is implemented in *DonkeyClient.run_task_chain* method.

# 4.Performance

Task execution and details are stored in process memory as instances of *TaskTracker* implemented in *master.py*, tasks can be retrieved to analyze performance.
For this we use the *DonkeyClient.retrieve_task_status* method.
A few manipulations with pandas can give us and overview proscessing time accross nodes, for each step of the process:

|            worker           | job     | duration (ms) |
|:---------------------------:|---------|:-------------:|
| http://164.92.219.216:8000/ | map     | 228           |
| http://164.92.219.239:8000/ | map     | 270           |
| http://164.92.219.238:8000/ | map     | 239           |
| http://164.92.219.216:8000/ | shuffle | 1234          |
| http://164.92.219.239:8000/ | shuffle | 1274          |
| http://164.92.219.238:8000/ | shuffle | 1024          |
| http://164.92.219.216:8000/ | reduce  | 140           |
| http://164.92.219.239:8000/ | reduce  | 122           |
| http://164.92.219.238:8000/ | reduce  | 137           |


The map reduce word count task took a total of *1684 ms* for *2.239 MB* file 

The same word_count done with a single step *word_count.py* script distributed accross the worker nodesgives the following results:
| worker                      | job        | duration (ms) |
|-----------------------------|------------|---------------|
| http://164.92.219.216:8000/ | word_count | 95            |
| http://164.92.219.239:8000/ | word_count | 113           |
| http://164.92.219.238:8000/ | word_count | 135           |

The full process is an order of magnitude faster with *135 ms* on the same input file.

# 5. Conclusion

The flexibility of this model enables specific implementations of tasks that can show better performance than a naive implementation of the MapReduce framework. This is mostly due to the fact that we are avoiding long running input/output tasks at each step of the MapReduce.

 
