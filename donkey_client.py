from typing import Dict
import requests
from pathlib import Path
import time

"""
Basic client library for Donkey distributed application.
"""


class DonkeyClient:
    def __init__(self, master_node: str):
        self._master_node = master_node
        self._session = requests.session()

    def submit_file(self, file_path: Path) -> dict:
        resp = requests.post(
            f"{self._master_node}/fs", files={"file": open(file_path, "rb")}
        )
        return resp.json()

    def submit_job(self, script_file_path: Path) -> dict:
        resp = requests.post(
            f"{self._master_node}/jobs", files={"file": open(script_file_path, "rb")}
        )
        return resp.json()

    def run_task(self, file_name, job_names) -> dict:
        resp = self._session.put(
            f"{self._master_node}/tasks",
            params={
                "job_names": job_names,
                "file_name": file_name,
            },
        )
        return resp.json()

    def run_task_chain(self, file_name, job_names):
        for job in job_names:
            resp = self.run_task(file_name, job)
            task_uids = [t["uid"] for t in resp]
            complete = False
            retries = 0
            while not complete and retries < 1000:
                status = [
                    self.retrieve_task_status(uid)["completed"] for uid in task_uids
                ]
                complete = all(status)
                # backing off for a moment
                time.sleep(0.5)
                retries += 1
            # then on to the next task
        # returning the response of the last task
        return resp

    def retrieve_task_status(self, task_id=None):
        resp = self._session.get(
            f"{self._master_node}/tasks",
            params={
                "task_id": task_id,
            },
        )
        return resp.json()

    def retrieve_files(self, file_name: str = None):
        resp = self._session.get(
            f"{self._master_node}/fs",
            params={
                "file_name": file_name,
            },
        )
        return resp.json()

    def cluster_info(self):
        resp = self._session.get(
            f"{self._master_node}/info",
        )
        return resp.json()


if __name__ == "__main__":
    # path to project
    SCRIPTS_PATH = Path(r"C:\Users\Marlo\projects\Python\INF727\examples\scripts")
    DATA_PATH = Path(r"C:\Users\Marlo\projects\Python\INF727\examples\data")

    dc = DonkeyClient("http://164.92.219.247:8000")

    dc.submit_file(DATA_PATH / "civil.txt")
    dc.submit_job(SCRIPTS_PATH / "map.py")
    dc.submit_job(SCRIPTS_PATH / "shuffle.py")
    dc.submit_job(SCRIPTS_PATH / "reduce.py")

    r = dc.run_task_chain("civil.txt", ["map", "shuffle", "reduce"])
    output_file = r[0]["output"]
    output = dc.retrieve_files(output_file)

    pass

    # from pathlib import Path
    # SCRIPTS_PATH = Path(r'C:\Users\Marlo\projects\Python\INF727\examples\scripts')
    # DATA_PATH = Path(r'C:\Users\Marlo\projects\Python\INF727\examples\data')
    #
    # from donkey_client import DonkeyClient
    #
    # dc = DonkeyClient('http://127.0.0.1:8000')
    #
    # dc.submit_file(DATA_PATH / "civil.txt")
    # dc.submit_job(SCRIPTS_PATH / "map.py")
    # dc.run_task("civil.txt", "shuffle")
