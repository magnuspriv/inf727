from fabric import Connection
from fabric.transfer import Transfer
import pathlib

TARGET_DIR = "/opt/donkey/"
hosts = [
    "164.92.219.247",  # master node
    "164.92.219.216",  # worker node 1
    "164.92.219.239",  # worker node 2
    "164.92.219.238",  # worker node 3
]


# Remote command execution via ssh logic is encapsulated in this Node class
class Node:
    def __init__(self, user, host):
        self._connection = Connection(f'{user}@{host}')

    def run(self, cmd: str, hide=False):
        return self._connection.run(cmd, hide=hide)

    def get(self, src, dest):
        return Transfer(self._connection).get(src, dest)

    def put(self, src, dest):
        self._connection.run(f'mkdir -p {dest.parent.as_posix()}')
        return Transfer(self._connection).put(src, dest.as_posix())


nodes = [Node("root", host) for host in hosts]

print('1- Connecting to remote hosts')
print('_____________________________')
for node in nodes:
    print(f'Connected to host {node.run("hostname").stdout.strip()}')
print('✓ Done')


# Purging previous deployment and creating the target directory structure
print('2- Creating directory structure')
print('_______________________________')
for node in nodes:
    node.run(f'rm -rf {TARGET_DIR}')
    print(f'On host {node.run("hostname").stdout.strip()}')
    node.run(
        f"sudo mkdir -p {TARGET_DIR}static {TARGET_DIR}templates {TARGET_DIR}var/data {TARGET_DIR}var/master {TARGET_DIR}var/scripts"
    )
print('✓ Done')

# Copying application files on the remote hosts
print('3- Copying project executable to remote hosts')
print('______________________________________________')
script_files = ["slave.py", "master.py", "req.txt", "config.py"]
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    for file in script_files:
        node.put(
            pathlib.Path(__file__).parent.joinpath(file),
            pathlib.Path(TARGET_DIR).joinpath(file),
        )
print('✓ Done')

# Copying static resources and html templates
print('4- Copying project static assets to remote hosts')
print('_________________________________________________')
static_dirs = ["static/donkey.png", "static/favicon.png", "templates/index.html"]
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    for _dir in static_dirs:
        node.put(
            pathlib.Path(__file__).parent.joinpath(_dir),
            pathlib.Path(TARGET_DIR).joinpath(_dir),
        )
print('✓ Done')

# Printing the content of remote hosts /opt/donkey folder to stdout
print('5- Checking the content of the deployment directory')
print('___________________________________________________')
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    print(node.run(f"ls {TARGET_DIR}").stdout)
print('✓ Done')

# Creating a python virtual environment, activating it and installing the project dependencies
print('6- Creating python virtual environment and installing dependencies')
print('_________________________________________________________________')
for node in nodes:
    print(f'On host {node.run("hostname").stdout.strip()}')
    node.run("sudo apt-get update -y")
    node.run("sudo apt install python3.8-venv -y")
    node.run(f"python3 -m venv {TARGET_DIR}env")
    node.run(f"source {TARGET_DIR}/env/bin/activate")
    node.run(f"{TARGET_DIR}env/bin/python -m pip install -r {TARGET_DIR}req.txt")
    node.run(f"{TARGET_DIR}env/bin/python -m pip list")
print('✓ Done')

# Reloading and starting web server services on hosts
print('7- Starting server processes as services')
print('___________________________________________________')
for node in nodes:
    node.run("sudo systemctl daemon-reload")
    node.run("sudo systemctl start donkey.service")
print('✓ Done')
