import datetime
import importlib
import logging
import os
import pathlib
import random
import sys
from concurrent.futures import ProcessPoolExecutor
from logging.config import dictConfig
from typing import Dict, Callable
from uuid import UUID

from fastapi import BackgroundTasks
from fastapi import FastAPI, File, UploadFile, HTTPException, Request
from fastapi.responses import HTMLResponse, FileResponse
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
import requests

import asyncio
from config import LogConfig

dictConfig(LogConfig().dict())
logger = logging.getLogger("worker")

VAR_DATA = pathlib.Path(__file__).parent / "var/data"
VAR_SCRIPTS = pathlib.Path(__file__).parent / "var/scripts"

# For local testing
# MASTER_NODE = "http://127.0.0.1:8000/"

MASTER_NODE = "http://164.92.219.247:8000/"

sys.path.append(str(VAR_SCRIPTS))

description = """
*A distributed application for Democrats 🇺🇸 🐎*

**Donkey Worker API (Used by the master node)**  

## fs
You can:
* **Write** files or blocks to file system
* **Read** files or blocks from the file system  

All data is stored locally on the worker in **./var/data/**

## jobs
You can:
* **Job list** list all worker registered jobs
* **Create jobs** by submitting a python source files `script.py` implementing a compute method with a file_path argument.
* **Run job** run job on worker  

All scripts are stored locally on the worker in **./var/scripts/**.  

**Use the tags below to test the worker API.**  
"""

tags_metadata = [
    {
        "name": "fs",
        "description": "Operations on the application's file system **writes** to nodes are handled here.",
    },
    {
        "name": "jobs",
        "description": "Cluster jobs, python scripts deployed to all worker nodes.",
    },
]


class Task(BaseModel):
    uid: str = None
    status: str = "in_progress"
    result: dict = None


tasks: Dict[str, Task] = {}

app = FastAPI(
    title="Donkey Worker",
    description=description,
    version="0.0.1",
    openapi_tags=tags_metadata,
    contact={
        "name": "Donkey",
    },
)

app.mount("/static", StaticFiles(directory=pathlib.Path(__file__).parent / "static"), name="static")


@app.get("/", response_class=HTMLResponse)
def root():
    return """
        <html>
        <head>
            <link rel="icon" href="/static/favicon.png">
            <style>
            .center {
              display: block;
              margin-left: auto;
              margin-right: auto;
            },
            </style>
            <title>Donkey</title>
        </head>
        <body>
            <h1 style="text-align:center">Donkey worker</h1>
            <p style="text-align:center">Donkey is ready to rumble!
            Check out the <a href="/docs/">api docs</a>.</p><br>
            <p></p>
            <p></p>
            <img class="center" src="/static/donkey.png", alt="donkey">
        </body>
    </html>
    """


@app.get("/fs", tags=["fs"])
def read_files(file_name: str = None):
    read_ex = HTTPException(
        status_code=400,
        detail=f"Could not retrieve {file_name} from file system",
    )

    files = [f for f in os.listdir(VAR_DATA)]

    if file_name:
        if file_name not in files:
            raise read_ex
        else:
            return FileResponse(
                VAR_DATA / file_name, media_type="text", filename=file_name
            )
    return files


# todo: make write async aw
@app.post("/fs", tags=["fs"])
async def write_file(file: UploadFile = File(...)):
    """
    Test here
    :param file:
    :return:
    """
    content = await file.read()
    with open(VAR_DATA / file.filename, "wb") as buffer:
        buffer.write(content)
    return {"path": f"{VAR_DATA / file.filename}", "status": "Success"}


@app.post("/jobs", tags=["jobs"])
async def create_job(file: UploadFile = File(...)):
    logger.debug(f"Writing script source file {file.filename}")

    content = await file.read()

    with open(VAR_SCRIPTS / file.filename, "wb") as buffer:
        buffer.write(content)

    return {"job_id": 1, "job_name": file.filename}


@app.get("/jobs", tags=["jobs"])
def read_jobs():
    jobs = [f for f in os.listdir(VAR_SCRIPTS)]
    return jobs


@app.delete("/jobs", tags=["jobs"])
def delete_job(job_name=None):
    return {"status": "Not implemented"}


@app.get("/ping")
def ping():
    return {"status": "Active", "ts": datetime.datetime.now()}


@app.get("/tasks")
async def status_handler(uid: UUID = None):
    if uid:
        return tasks[uid]
    else:
        return tasks


@app.put("/tasks")
def run_task(
    request: Request,
    background_tasks: BackgroundTasks,
    job_name=None,
    file_name=None,
    task_id=None,
):
    job_module = job_name.split(".py")[0]
    available_jobs = {f.split(".py")[0] for f in os.listdir(VAR_SCRIPTS)}

    if job_module not in available_jobs:
        ex = HTTPException(
            status_code=404,
            detail=f"Could not find or run job {job_name}",
        )
        raise ex

    imported_module = importlib.import_module(job_module)
    importlib.reload(imported_module)
    cluster_info = requests.get(f"{MASTER_NODE}info").json()

    new_task = Task(uid=task_id)
    tasks[new_task.uid] = new_task

    input_file_path = VAR_DATA / file_name
    output_file_name = f"{job_name}_{file_name}"
    output_file_path = VAR_DATA / output_file_name

    background_tasks.add_task(
        start_task,
        imported_module.compute,
        new_task.uid,
        input_file_path,
        output_file_path,
        cluster_info,
        MASTER_NODE,
        str(request.base_url),
    )

    return {
        "job_id": random.randint(1000, 2000),
        "file": output_file_name,
        "status": "running...",
    }


"""
EVENTS
______
"""


# Set app executor as ProcessPoolExecutor for CPU bound tasks
@app.on_event("startup")
async def startup_event():
    app.state.executor = ProcessPoolExecutor()


async def run_in_process(fn, *args, **kwargs):
    print('get event loop')
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(app.state.executor, fn, *args)


async def start_task(fn: Callable, uid: UUID, *args) -> None:
    print('run in process')
    await run_in_process(fn, *args)
    tasks[uid].result = 0
    tasks[uid].status = "complete"
    requests.post(f"{MASTER_NODE}tasks", params={"task_id": uid})


def write_ping():
    with open(VAR_DATA / "ping.dt", "w") as f:
        f.write(str())
    return 0


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

# TODO: Ok so now we have reduced results, we need a way to join all the results in a way that makes sense everything.