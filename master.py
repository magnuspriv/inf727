import ast
import io
import logging
import os
import pathlib
import pickle
from datetime import datetime
from hashlib import sha1
from itertools import cycle
from logging.config import dictConfig
from typing import Dict, List
from collections import defaultdict
import httpx
from fastapi import FastAPI, File, UploadFile, HTTPException, Request
from fastapi.responses import HTMLResponse, StreamingResponse, Response, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel, Field
import json

from config import LogConfig

# add a cluster
dictConfig(LogConfig().dict())
logger = logging.getLogger("master")

VAR_DATA = pathlib.Path(__file__).parent / "var/master"

# For local testing
# WORKERS = ["http://127.0.0.1:8001/", "http://127.0.0.1:8002/", "http://127.0.0.1:8003/"]

WORKERS = ["http://164.92.219.216:8000/", "http://164.92.219.239:8000/", "http://164.92.219.238:8000/"]

REPLICA_FACTOR = 1

# A task is an running instance of a job
TASKS = {}
CHUNK_SIZE = 2 ** 20


description = """
*A distributed application for Democrats 🇺🇸 🐎*

**Donkey Master API**  

## fs
You can:
* **Write** files or blocks to file system
* **Read** files or blocks from the file system  

All data is stored locally on the worker in **./var/data/**

## jobs
You can:
* **Job list** list all worker registered jobs
* **Create jobs** by submitting a python source files `script.py` implementing a compute method with a file_path argument.
* **Run job** run job on worker  

All scripts are stored locally on the worker in **./var/scripts/**.  

**Use the tags below to test the worker API.**  
"""


tags_metadata = [
    {
        "name": "filesystem",
        "description": "Operations on the application's file system **writes** to nodes are handled here.",
    },
    {
        "name": "jobs",
        "description": "Cluster wide executable python scripts (deployed to all worker nodes).",
    },
    {
        "name": "tasks",
        "description": "Tasks are instances of running jobs.",
    },
    {
        "name": "info",
        "description": "Information on cluster state and running nodes.",
    },
]


class TaskTracker(BaseModel):
    uid: str = None
    completed: bool = False
    start: datetime = Field(default_factory=datetime.now)
    end: datetime = None
    duration: float = None
    job: str = None
    file: str = None
    worker: str = None
    output: str = None
    output_block: str= None


app = FastAPI(
    title="Donkey Master Node",
    description=description,
    version="0.0.1",
    #  terms_of_service="http://example.com/terms/",
    openapi_tags=tags_metadata,
    contact={
        "name": "Donkey",
    },
)

app.mount("/static", StaticFiles(directory=pathlib.Path(__file__).parent / "static"), name="static")
templates = Jinja2Templates(directory=str(pathlib.Path(__file__).parent / "templates"))
task_tracking: Dict[str, TaskTracker] = dict()

"""
Helper Functions
________________
"""


def _files_per_node(worker_nodes: List[str]):
    master_blocks = [
        pickle.load(open(VAR_DATA / f, "rb"))
        for f in os.listdir(VAR_DATA)
        if f.endswith(".blocks")
    ]
    files_per_node: Dict[str, Dict[str, List["str"]]] = dict()
    for worker in worker_nodes:
        files_per_node[worker] = defaultdict(list)
    for block_file in master_blocks:
        for blocks in block_file["blocks"]:
            block_name, worker = blocks
            files_per_node[worker][block_file["file_name"]].append(block_name)

    return files_per_node


def _unique_file_blocks(file_name):
    files = [
        pickle.load(open(VAR_DATA / f, "rb"))
        for f in os.listdir(VAR_DATA)
        if f.endswith(".blocks")
    ]

    read_ex = HTTPException(
        status_code=400,
        detail=f"Could not retrieve {file_name} from file system",
    )

    file_names = {b["file_name"] for b in files}

    if file_name not in file_names:
        raise read_ex

    unique_blocks = {}

    for files in files:
        if files["file_name"] == file_name:
            unique_blocks = {
                block_name: worker_node for block_name, worker_node in files["blocks"]
            }

    return unique_blocks


def _is_pickle(stream):
    try:
        pickle.loads(stream)
        return True
    except pickle.UnpicklingError:
        return False


"""
Master node's main page
_______________________
"""


@app.get("/", response_class=HTMLResponse, include_in_schema=False)
async def index(request: Request):
    async with httpx.AsyncClient() as client:
        worker_status = {
            worker: (await client.get(f"{worker}ping")).json() for worker in WORKERS
        }
    return templates.TemplateResponse(
        "index.html",
        {
            "request": request,
            "files": _files_per_node(WORKERS),
            "jobs": [f for f in os.listdir(VAR_DATA) if f.endswith(".py")],
            "workers": WORKERS,
            "worker_status": worker_status,
            "now": datetime.now(),
            "cluster_config": {
                "Replication Factor": REPLICA_FACTOR,
                "Block Size (Bytes)": CHUNK_SIZE,
            },
        },
    )


"""
File system operations API
__________________________
"""


@app.get("/fs", tags=["filesystem"])
async def read_files(file_name: str = None):
    # files = [f.split('.blocks')[0] for f in os.listdir(VAR_DATA)]

    if file_name:

        read_ex = HTTPException(
            status_code=400,
            detail=f"Could not retrieve {file_name} from file system",
        )
        files = [
            pickle.load(open(VAR_DATA / f, "rb"))
            for f in os.listdir(VAR_DATA)
            if f.endswith(".blocks")
        ]
        file_names = {b["file_name"] for b in files}

        if file_name not in file_names:
            raise read_ex
        file_parts: list = []
        for file in files:
            if file["file_name"] == file_name:
                unique_blocks = {
                    block_name: worker_node
                    for block_name, worker_node in file["blocks"]
                }

                for block_name, worker_node in unique_blocks.items():
                    async with httpx.AsyncClient() as client:
                        resp = await client.get(
                            f"{worker_node}fs",
                            params={
                                "file_name": block_name,
                            },
                        )
                        file_parts.append(resp.content)

        buffer = None
        if _is_pickle(file_parts[0]):
            output_dicts = [pickle.loads(part) for part in file_parts]
            resp_dict = dict()
            for dic in output_dicts:
                resp_dict.update(dic)

            content_type = "json"
            content = resp_dict
        else:
            buffer = io.BytesIO(b"".join(file_parts))
            content_type = "text"
            content = buffer.read().decode()

        return JSONResponse(
            content={
                "file_name": file_name,
                "content_type": content_type,
                "content": content,
            }
        )

        # TODO:join blocks into a single file

        return
    else:
        return [
            pickle.load(open(VAR_DATA / f, "rb"))
            for f in os.listdir(VAR_DATA)
            if f.endswith(".blocks")
        ]


@app.post("/fs", tags=["filesystem"])
async def write_file(
    request: Request,
    file: UploadFile = File(None),
    target_worker: int = None,
    register_file: str = None,
    register_block: str = None,
):
    """
    Writes the uploaded file to the applications distributed file system.

    | Argument      | Description |
    | ----------- | ----------- |
    | **target_worker**      | when the optional  is specified, writes the uploaded file as a single block into the targeted worker's file system with no replication.       |
    | **file**   | Text file to be written in the distributed file system.        |
    \f
    :param target_worker: When specified, writes the uploaded file as a single block into the targeted
    worker's file system with no replication.
    :param file: User input.
    """

    blocks = dict()
    file_name = file.filename if file else register_file
    blocks["file_name"] = file_name
    blocks["blocks"] = []

    logger.info(f"Received file {file_name}")
    # Registers a file to the file system without moving it
    if target_worker and register_file:
        target_worker_url = WORKERS[target_worker - 1]
        blocks["file_name"] = register_file
        blocks["blocks"].append((register_block or register_file, target_worker_url))

    # Transfer file to a specific node (useful for shuffle like jobs)
    elif target_worker and not register_file:
        target_worker_url = WORKERS[target_worker - 1]
        logger.info(f"Writing {file_name} to node {target_worker}")
        content = await file.read()
        block_file_name = file_name
        f = io.BytesIO(content)
        f.name = file_name
        async with httpx.AsyncClient() as client:
            r = await client.post(f"{target_worker_url}fs", files={"file": f})
        blocks["blocks"].append((block_file_name, target_worker_url))

    # Otherwise just distribute block to workers according to REPLICA_FACTOR (using their Rest API)
    else:
        block_counter = 0
        cycle_workers = cycle(WORKERS)
        content_len = int(request.headers["content-length"])
        while content := await file.read(size=content_len // len(WORKERS)):
            block_counter += 1
            print(f"Processing block #{block_counter}")
            block_file_name = f"{file_name}-block-{block_counter:05}"
            block_file_path = VAR_DATA / block_file_name
            logger.info(f"Saving block {block_file_path} to disk.")
            f = io.BytesIO(content)
            f.name = block_file_name
            logger.info(f"Distributing block to workers.")
            for _ in range(REPLICA_FACTOR):
                worker = next(cycle_workers)
                async with httpx.AsyncClient() as client:
                    r = await client.post(f"{worker}fs", files={"file": f})
                # save block location
            blocks["blocks"].append((block_file_name, worker))
    dmp_file = VAR_DATA / f"{file_name}.blocks"

    if os.path.isfile(dmp_file):
        with open(dmp_file, "rb") as f:
            existing_blocks = pickle.load(f)
            if set(blocks["blocks"]) - set(existing_blocks["blocks"]):
                blocks["blocks"] += existing_blocks["blocks"]

    with open(dmp_file, "wb") as f:
        pickle.dump(blocks, f)

    return blocks


@app.delete("/fs", tags=["filesystem"])
async def delete_file(file_name):
    return {"Status": "Not implemented"}


"""
Job scheduler API
__________________________
"""


@app.post("/jobs", tags=["jobs"])
async def create_job(file: UploadFile = File(...)):
    logger.info(f"Received file {file.filename}")
    create_ex = HTTPException(
        status_code=400,
        detail="Requires valid python scripts "
        "with a module top-level `compute` function requiring "
        "no arguments.",
    )

    try:
        content = await file.read()
        logger.debug(f"Parsing {file.filename}...")
        # Try to parse python file
        tree = ast.parse(content)
        logger.debug(f"{file.filename} is a valid python script")
    except SyntaxError:
        raise create_ex

    # Checking if the python source files contains a compute method
    func_def_nodes = {
        node.name: node for node in tree.body if isinstance(node, ast.FunctionDef)
    }

    compute_fun = func_def_nodes.get("compute", None)
    if not compute_fun:
        raise create_ex
    script_file_path = VAR_DATA / file.filename

    with open(script_file_path, "wb") as buffer:
        buffer.write(content)

    f = io.BytesIO(content)
    f.name = file.filename
    for worker in WORKERS:
        async with httpx.AsyncClient() as client:
            r = await client.post(f"{worker}jobs", files={"file": f})
            f.seek(0)

    return {"job_name": file.filename, "worker_nodes": WORKERS}


@app.get("/tasks", tags=["tasks"])
def current_tasks(task_id=None):
    if task_id:
        return task_tracking[task_id]
    else:
        return task_tracking


@app.post("/tasks", tags=["tasks"])
def task_done(task_id):
    task_tracker = task_tracking[task_id]
    task_tracker.completed = True
    task_tracker.end = datetime.now()

    duration_ms = round((task_tracker.end - task_tracker.start).total_seconds() * 1000)
    task_tracker.duration = duration_ms
    print(f"{task_id} done, took {duration_ms} ms")
    return


@app.put("/tasks", tags=["tasks"])
async def run_task(job_names=None, file_name=None, aggregation_function="sum"):
    file_blocks = _unique_file_blocks(file_name)
    tasks = []
    for job_name in job_names.split(","):
        # schedule the job on each block, then aggregate  somehow ?
        for block, worker in file_blocks.items():
            task_tracker = TaskTracker()
            task_tracker.uid = f"{job_name}_{sha1(worker.encode()).hexdigest()[:10]}"
            task_tracker.file = file_name
            task_tracker.job = job_names
            task_tracker.worker = worker

            async with httpx.AsyncClient() as client:
                try:
                    resp: httpx.Response = await client.put(
                        f"{worker}tasks",
                        params={
                            "job_name": job_name,
                            "file_name": block,
                            "task_id": task_tracker.uid,
                        },
                    )
                    resp.raise_for_status()
                    task_tracker.output = f"{job_name}_{file_name}"
                    task_tracker.output_block = resp.json()["file"]
                    tasks.append(task_tracker)
                    task_tracking[task_tracker.uid] = task_tracker

                except httpx.RequestError as exc:
                    ex = HTTPException(
                        status_code=500,
                        detail=f"Error: Internal Server Error",
                    )
                    raise ex

                except httpx.HTTPStatusError:
                    ex = HTTPException(
                        status_code=404,
                        detail=f"Error: Could not find or run the requested job {job_name}",
                    )
                    raise ex

    return tasks


@app.get("/info", tags=["info"])
def cluster_info():
    return {worker: i for i, worker in enumerate(WORKERS, 1)}


@app.delete("/jobs", tags=["jobs"])
def delete_job(job_name=None):
    return {"Status": "Not implemented"}


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
