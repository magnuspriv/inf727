import os
import pickle
from collections import Counter
from pathlib import Path
from typing import List, Tuple

import requests


def compute(
    input_file_path: Path, output_file_path, cluster_info, master_node, worker_url
):
    this_node: int = int(cluster_info[worker_url])
    var_dir: Path = input_file_path.parent
    shuffled_map: List[Tuple[str, int]] = []
    for f in os.listdir(var_dir):
        print(f"processing {f}")
        if f.startswith("shuffle") and f.endswith(f"node{this_node}"):
            with open(var_dir / f, "rb") as f:
                shuffled_map += pickle.load(f)

    if not shuffled_map:
        raise ValueError("No reduce files available for shuffle task")

    reduced_map = Counter([k for k, v in shuffled_map])

    node_output_file_path: Path = Path(
        str(output_file_path)
    )
    with open(node_output_file_path, "wb") as f:
        pickle.dump(reduced_map, f)
    block_name = node_output_file_path.stem + node_output_file_path.suffix
    file_name = block_name.split('-')[0]
    resp = requests.post(
        f"{master_node}fs",
        params={
            "target_worker": this_node,
            "register_file": file_name,
            "register_block": block_name
        },
    )
    return 0
