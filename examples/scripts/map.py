import re
import pickle
from pathlib import Path
import requests
import hashlib


# TODO: change pickle to json perf ?
def compute(input_file_path, output_file_path, cluster_info, master_node, worker_url):
    print('Running map')
    this_node: int = int(cluster_info[worker_url])
    block_name = output_file_path.stem + output_file_path.suffix
    file_name = str(block_name).split('-')[0]
    print('opening file from input path')
    with open(input_file_path, 'r') as f:
        str_input = f.read()
    print('split')
    words = re.split(r"\W+", str_input)

    result = [(word, 1) for word in words]
    print('dumping pickle')
    with open(Path(str(output_file_path)), 'wb') as f:
        pickle.dump(result, f)
    print('posting to master')
    resp = requests.post(
        f"{master_node}fs",
        params={
            "target_worker": this_node,
            "register_file": file_name,
            "register_block": block_name
        },
    )
    return result


