from collections import Counter
import re
import pickle


def compute(input_file_path, output_file_path, cluster_info, master_node, worker_url):
    with open(input_file_path, 'r') as f:
        str_input = f.read()
    word_count = Counter(re.split(r"\W+", str_input))
    result = dict(sorted(word_count.items(), key=lambda x: x[1], reverse=True))
    with open(output_file_path, 'wb') as f:
        pickle.dump(result, f)
    return result

