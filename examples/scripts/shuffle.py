import hashlib
import os
import pickle
from collections import defaultdict
from pathlib import Path
from typing import Dict
from typing import List, Tuple
import io
import requests


def compute(
    input_file_path: Path, output_file_path, cluster_info, master_node, worker_url
):
    this_node: int = int(cluster_info[worker_url])
    var_dir: Path = input_file_path.parent
    unsorted_map: List[Tuple[str, int]] = None
    for f in os.listdir(var_dir):
        if f.startswith("map") and not f.endswith('blocks'):
            with open(var_dir / f, "rb") as f:
                unsorted_map = pickle.load(f)
    if not unsorted_map:
        raise ValueError("No map files available for shuffle task")

    shuffle_groups: Dict[int, List[Tuple[str, int]]] = defaultdict(list)
    for key, value in unsorted_map:
        target_node = (
            int(hashlib.sha1(key.encode("utf-8")).hexdigest(), 16) % len(cluster_info)
        ) + 1
        shuffle_groups[target_node].append((key, value))
    for node, shuffle in shuffle_groups.items():
        node_output_file_path: Path = Path(
            str(output_file_path) + f"_node{this_node}_node{node}"
        )
        if node == this_node:
            with open(node_output_file_path, "wb") as f:
                pickle.dump(shuffle, f)
            resp = requests.post(
                f"{master_node}fs",
                params={
                    "target_worker": node,
                    "register_file": node_output_file_path.stem
                    + node_output_file_path.suffix,
                },
            )
        else:
            content: bytes = pickle.dumps(shuffle)
            f = io.BytesIO(content)
            f.name = (
                output_file_path.stem
                + output_file_path.suffix
                + f"_node{this_node}_node{node}"
            )
            resp = requests.post(
                f"{master_node}fs", params={"target_worker": node}, files={"file": f}
            )
    return 0
