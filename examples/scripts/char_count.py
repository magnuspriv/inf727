from collections import Counter


def compute(file_path) -> dict:
    with open(file_path, 'rb') as f:
        str_input = f.read()
    word_count = Counter(str_input)
    return dict(sorted(word_count.items(), key=lambda x: x[1], reverse=True))
